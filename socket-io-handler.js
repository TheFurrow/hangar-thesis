var socket = io('/website', { 'query': { 'auth': '0459m8y4b5um89y4b58'}});

$(document).ready(function() {
	console.log("Document loaded fully.");
	
	socket.on('viewerAdd', function(msg) {
		console.log(msg);
		
		//Check length of msg and query elements in #viewers, remove from array if found from elements
		for(var i = msg.length - 1; i >= 0; i--)
		//for(var i = 0; i <= msg.length; i++)
		{
			console.log("------------ LOOP %s ---------------", i);
			console.log("Loop size: %s", msg.length);
			if( $("#viewers").find('div[data-viewers-id="'+ msg[i].id +'"]').length > 0 )
			{
				console.log("Found element for %s", msg[i].id);
				console.log("Ignoring this value");
				msg.pop(msg[i]);
			}
			else {
				console.log("Did not found element for %s", msg[i].id);
				console.log("Appending new element");
				$('#viewers').append('<div data-viewers-id="'+msg[i].id+'"><p>Viewer #'+msg[i].amount+'</p></div>');
			}
			
			console.log("----------- LOOPEND %s --------------", i);
		}
	});
	
	socket.on('playerAdd', function(msg) {
		$('#players').append('<div data-player-id="'+msg.id+'"><p>Player #'+msg.amount+'</p></div>');
	});
	
	socket.on('removeSession', function(msg) {
		console.log("Remove session called for %s", msg)
		$('div[data-player-id="'+msg+'"]').remove();
		$('div[data-viewers-id="'+msg+'"]').remove();
	});
});
