var playerPos;

var canvasMap;
var canvasPlayers;
var canvasMapContext;
var canvasPlayersContext;

$(document).ready(function() {
	
	canvasPlayers = document.getElementById("mapPlayers");
	canvasMap = document.getElementById("map");		
	
	canvasMapContext = canvasMap.getContext("2d");
	canvasMapContext.translate(1000 / 2, 1000 / 2);
	
	canvasPlayersContext = canvasPlayers.getContext("2d");
	canvasPlayersContext.translate(1000 / 2, 1000 / 2);
	

	function drawMap()
	{
		var img = document.getElementById("level1topdown");
		//canvasMapContext.getContext("2d");
		canvasMapContext.drawImage(img,(img.clientWidth / 2 * -1) ,(img.clientHeight / 2 * -1));
	}
	
	//Call function after constructing
	drawMap();
	
	//This function draws our players on the canvas with fixed scale of coordinates
	function drawPlayer(playerPos)
	{
		//Store player positions and adjust the values for canvas size
		var playerX = playerPos.pos.X / 16;
		var playerY = playerPos.pos.Y / 16;
		
		var img = document.getElementById("playerIcon");
		
		//Begin drawing
		canvasPlayersContext.save();
		canvasPlayersContext.beginPath();
		
		//Make translations and rotations for the icon based on players yaw rotation
		
		//Positive translation
		canvasPlayersContext.translate(playerX, playerY);
		
		//Convert degrees into radians
		canvasPlayersContext.rotate((Math.PI / 180) * playerPos.rot.Yaw);
		
		//Make negative translation
		canvasPlayersContext.translate(-playerX, -playerY);
		
		//Finally draw the icon
		canvasPlayersContext.drawImage(img, playerX - 10, playerY - 10, 20, 20);
		canvasPlayersContext.restore();
	}
	
	//Receive players and modify them before sending to draw process
	function receivePlayerPos(newPlayerPos) {
		playerPos = newPlayerPos;

		drawPlayerPosition(playerPos);
	};

	//Draw players in canvas
	function drawPlayerPosition(playerPos) {
		if(canvasPlayers != undefined && canvasPlayersContext != undefined)
		{
			//Clear all rects on each update
			canvasPlayersContext.clearRect(0,0, -1000 / 2, 1000 / 2);
			canvasPlayersContext.clearRect(0,0, 1000 / 2, -1000 / 2);
			canvasPlayersContext.clearRect(0,0, -1000 / 2, -1000 / 2);
			canvasPlayersContext.clearRect(0,0, 1000 / 2, 1000 / 2);
			
			//Start processing players from the list
			for(var i = 0; i < playerPos.length;i++) {
				drawPlayer(playerPos[i]);
			}
		}
	};
	
	//Receives player positions from 
	socket.on('updatePlayerPos', function(newPos) {
		receivePlayerPos(newPos);
	});

});
