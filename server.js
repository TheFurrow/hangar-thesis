var express = require('express');
var app = express();
var http = require('http').Server(app);
var io = require('socket.io')(http);

var serverConfig = require('./server.config.js');

var port = process.env.PORT || process.env.OPENSHIFT_NODEJS_PORT || 8080,
    ip   = process.env.IP   || process.env.OPENSHIFT_NODEJS_IP || '0.0.0.0';
	
app.use('/', express.static(__dirname));

//** FOR SOCKET **//

var websiteConnections = [];
var playerConnections = [];

var playerPositions = [];

var websiteNSP = io.of('/website');
var playersNSP = io.of('/players');


function checkIfExists(playerPos, socketId, callback)
{
	var confirmedPos = {'id':socketId, 'pos': playerPos.pos, 'rot':playerPos.rot};
	console.log(confirmedPos);
	
	loop: for(var i = 0; i <= playerPositions.length; i++) {
		//console.log("--------------START LOOP FOR %s & %s-------------", (i + "/" + (playerPositions.length)), playerPos.id.id);
	//console.log(playerPositions);
			if(playerPositions.length == 0) {
				//console.log(false);
				//console.log(confirmedPos);
				playerPositions.push(confirmedPos);
				//console.log("--------------END LOOP FOR %s-------------", (i + "/" + (playerPositions.length)));
				break loop;
			}
			else {
				
				//Check if id already exists on playerPositions

				//Fix the integer so this loop wont break down
				var newI = (i > 0) ? (i - 1) : i;
				if(socketId == playerPositions[newI].id)
				{
					//console.log("inner"+true);
					playerPositions.splice(newI, 1, confirmedPos);
					//console.log("--------------END LOOP FOR %s-------------", (i + "/" + (playerPositions.length)));
					break loop;

				}
				else if(i == playerPositions.length) {
					playerPositions.push(confirmedPos);
					break;
				}
			}
	}
	
	return callback();
}

//Authorize connection
websiteNSP.use(function(socket, next){
	if(socket != undefined)
	{
		if(socket.handshake.query.auth == serverConfig.secret) {
			console.log('Authorized connection for %s', socket.id);
			next();
			return;
		}
		
		console.log('Unauthorized connection');
		
		return;
	}
	else {
		console.log('Unauthorized connection');
		
		return;
	}
});

websiteNSP.on('connection', function(socket) {
	console.log("Wandering viewer joined session");
	
	//Disconnection
	socket.on('disconnect', function() {
		console.log(socket.id + ' have disconnected');
		websiteConnections.pop(socket.id);
		websiteNSP.emit('removeSession', socket.id);
	});
	
	/** Website **/
	
	//Add new player
	var newViewer = {
		id: socket.id,
		amount: websiteConnections.length + 1
	};
	
	websiteConnections.push(newViewer);
	
	console.log(websiteConnections);
	
	websiteNSP.emit('viewerAdd', websiteConnections);
	
	/*---------------------------------*/
	


});

playersNSP.use(function(socket, next) {
	//console.log(socket);
	//console.log(socket.Server.nsp);
	if(socket != undefined)
	{
		//console.log(socket);
		if(socket.handshake.query.auth == serverConfig.secret) {
			console.log('Authorized connection for %s', socket.id);
			next();
			return;
		}
		
		console.log('Unauthorized connection');
		
		return;
	}
	else {
		console.log('Unauthorized connection');
		
		return;
	}
});

playersNSP.on('connection', function(socket){
	console.log("Might player joined session");
	
	playerConnections.push(socket.id);
	
	/** Website **/
	
	//Add new player
	var newPlayer = {
		id: socket.id,
		amount: playerConnections.length
	};
	
	
	
	//Emit to website
	websiteNSP.emit('playerAdd', newPlayer);
	
	/** Hangar **/
	
	//Update position
	socket.on('playerPosition', function(playerPos) {
		//console.log(playerPos);
		checkIfExists(playerPos, socket.id, function(result) {
				//playerPositions.push(result);
				//console.log(playerPositions);
				websiteNSP.emit('updatePlayerPos', playerPositions);
		});
		
	});
	
	/*---------------------------------------*/
	
	//Disconnection
	socket.on('disconnect', function() {
		playerConnections.splice(playerConnections.indexOf(socket.id));
		
		
		playerPositions.splice(playerPositions.indexOf({'id':socket.id}));
		
		//Make sure to empty
		if(playerConnections.length == 0)
		{
			playerPositions = [];
		}
		
		//console.log(playerPositions);
		//Update views
		websiteNSP.emit('removeSession', socket.id);
		websiteNSP.emit('updatePlayerPos', playerPositions);
		
		console.log(socket.id + ' have disconnected');
	});

});

/** Socket - Common **/
io.on('connection', function(socket) {
	
	
});

app.get('/', function(req,res) {
	res.render("index");
});

http.listen(port, ip, function() {
	console.log("Server running on %s:%s", ip, port);
});